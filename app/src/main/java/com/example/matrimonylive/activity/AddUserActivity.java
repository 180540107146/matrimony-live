package com.example.matrimonylive.activity;

import android.os.Bundle;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;

import com.example.matrimonylive.R;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddUserActivity extends BaseActivity {
    @BindView(R.id.etActName)
    TextInputEditText etActName;
    @BindView(R.id.etActFatherName)
    TextInputEditText etActFatherName;
    @BindView(R.id.etActSurname)
    TextInputEditText etActSurname;
    @BindView(R.id.rbActFemale)
    MaterialRadioButton rbActFemale;
    @BindView(R.id.rbActMale)
    MaterialRadioButton rbActMale;
    @BindView(R.id.rgActGender)
    RadioGroup rgActGender;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_user_activity);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_add_user), false);

    }
}
