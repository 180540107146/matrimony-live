package com.example.matrimonylive.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.example.matrimonylive.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DashboardActivity extends BaseActivity {

    @BindView(R.id.cvActRegistration)
    CardView cvActRegistration;
    @BindView(R.id.cvActList)
    CardView cvActList;
    @BindView(R.id.cvActFavorite)
    CardView cvActFavorite;
    @BindView(R.id.cvActSearch)
    CardView cvActSearch;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        setUpActionBar(getString(R.string.lbl_dashboard), false);

    }

    @OnClick(R.id.cvActRegistration)
    public void onCvActRegistrationClicked() {
        Intent intent = new Intent(this, AddUserActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActList)
    public void onCvActListClicked() {
    }

    @OnClick(R.id.cvActFavorite)
    public void onCvActFavoriteClicked() {
    }

    @OnClick(R.id.cvActSearch)
    public void onCvActSearchClicked() {
    }
}
